// Replace all the placeholders (starting with "<" and ending in ">") with the correct values

// Neopixel
#define PIN            13
#define NUMPIXELS      14

// Wifi
char* SSID = "TheWorld";
char* WiFiPassword = "Kopenhagen12";
char* WiFiHostname = "DoorSign";

// MQTT
const char* mqtt_server = "192.168.1.12";
int mqtt_port = 1883;
const char* mqttUser = "";
const char* mqttPass = "";

// Device Specific Topics
const char* commandlTopic = "light/doorSign";
const char* stateTopic = "light/doorSign/state";
const char* rgbCommandTopic = "light/doorSign/rgb";
const char* rgbStateTopic = "light/doorSign/rgb/state";
const char* availabilityTopic = "light/doorSign/availability";
const char* recoveryTopic = "light/doorSign/recovery";
